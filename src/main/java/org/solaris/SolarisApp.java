package org.solaris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Arturooo
 * 
 *         Main entry to Solaris
 *
 */
@SpringBootApplication
public class SolarisApp {


	public static void main(String[] args) {
		SpringApplication.run(SolarisApp.class);

	}

}
