package org.solaris.controller;

import javax.servlet.http.HttpServletRequest;

import org.solaris.model.calculation.Calculation;
import org.solaris.service.ICalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CalculationController {
	
	@Autowired
	ICalculationService calcService;

	@PostMapping(value="/calculation")
	public Calculation serveCalculation(@RequestBody Calculation calc, HttpServletRequest request) {

		String ip = request.getRemoteAddr();
		Calculation processedCalculation = calcService.serveCalculation(calc, ip);
		
		return processedCalculation;

	}
	

}
