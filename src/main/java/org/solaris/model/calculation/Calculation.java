package org.solaris.model.calculation;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Calculation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String city;
	private String street;
	private Integer houseNumber;
	private LocalDate date;
	private String ip;
	private Double lat;
	private Double lon;
	private CalculatiomStatus status;

	public Calculation(String city, String street, Integer houseNumber, LocalDate date, String ip, Double lat,
			Double lon, CalculatiomStatus status) {
		super();
		this.city = city;
		this.street = street;
		this.houseNumber = houseNumber;
		this.date = date;
		this.ip = ip;
		this.lat = lat;
		this.lon = lon;
		this.status = status;
	}

	public Calculation() {
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(Integer houseNumber) {
		this.houseNumber = houseNumber;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public CalculatiomStatus getStatus() {
		return status;
	}

	public void setStatus(CalculatiomStatus status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	};

}
