package org.solaris.service;

import java.time.LocalDate;

import org.solaris.model.calculation.CalculatiomStatus;
import org.solaris.model.calculation.Calculation;
import org.solaris.model.calculation.CalculationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculationService implements ICalculationService {

	@Autowired
	CalculationRepository repro;

	/* (non-Javadoc)
	 * @see org.solaris.service.ICalculationService#serveCalculation(org.solaris.model.calculation.Calculation, java.lang.String)
	 */
	@Override
	public Calculation serveCalculation(Calculation calc, String ip) {

		calc.setDate(LocalDate.now());
		calc.setIp(ip);

		// TODO => Overthink this process 
		calc.setStatus(CalculatiomStatus.RECEIVED);

		final Calculation receivedCalc = repro.save(calc);
		return receivedCalc;
	}

}
