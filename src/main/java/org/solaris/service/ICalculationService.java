package org.solaris.service;

import org.solaris.model.calculation.Calculation;

public interface ICalculationService {

	Calculation serveCalculation(Calculation calc, String ip);

}